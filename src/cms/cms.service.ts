import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Page, PageDocument } from './schemas/page.schema';
import { CreatePageDTO } from './dto/create-page.dto';

@Injectable()
export class CMSService {
  constructor(@InjectModel(Page.name) private pageModel: Model<PageDocument>) {}

  async getPageById(pageId: string): Promise<Page> {
    return this.pageModel.findOne({ _id: pageId });
  }

  async createPage(createPageDTO?: CreatePageDTO): Promise<Page> {
    const createdPage = new this.pageModel(createPageDTO);
    return createdPage.save();
  }

  async findAll(): Promise<Page[]> {
    return this.pageModel.find().exec();
  }
}
