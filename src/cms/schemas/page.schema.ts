import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PageDocument = Page & Document;

@Schema()
export class Page {
  @Prop()
  name: string;

  @Prop({ required: true, type: 'object' })
  content: any;
}

export const PageSchema = SchemaFactory.createForClass(Page);
