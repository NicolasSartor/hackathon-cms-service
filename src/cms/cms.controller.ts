import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { CMSService } from './cms.service';
import { Page } from './schemas/page.schema';
import { CreatePageDTO } from './dto/create-page.dto';

@Controller('cms')
export class CMSController {
  constructor(private readonly cmsService: CMSService) {}

  @Get('/pages')
  getAll(): Promise<Page[]> {
    return this.cmsService.findAll();
  }

  @Get('/pages/:pageId')
  getPage(@Param('pageId') pageId: string): Promise<Page> {
    return this.cmsService.getPageById(pageId);
  }

  @Post('/pages')
  createPage(@Body() createPageDTO: CreatePageDTO): Promise<Page> {
    console.log('dto', createPageDTO);
    return this.cmsService.createPage(createPageDTO);
  }
}
