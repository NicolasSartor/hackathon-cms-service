export class CreatePageDTO {
  public readonly name: string;

  public readonly content: any;
}
